FROM node:16.13.2

RUN apt -y update && apt -y install postgresql nano

WORKDIR /app
COPY . .
RUN ls -la && pwd && yarn --version && yarn

ARG ENVIRONMENT_RUN=dev
ENV ENVIRONMENT_RUN $ENVIRONMENT_RUN
ENTRYPOINT yarn $ENVIRONMENT_RUN