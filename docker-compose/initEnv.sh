#!/bin/bash
# newgrp docker
set -e
source ./.env

eval $(ssh-agent)
ssh-add $SSH_KEY_PATH
git clone https://gitlab.com/crfluiz/monaco.git || (cd monaco && git checkout master && git pull && cd ..)
# rsync -avzh ../../monaco/ ./monaco/
cp monaco.Dockerfile ./monaco/Dockerfile

docker-compose up -d --build --force-recreate
docker-compose logs -f